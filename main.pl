human(X) :-
    man(X);
    woman(X).

man(X) :-
    X = sasha;
    X = grisha;
    X = vova;
    X = vasiliy1;
    X = vasiliy2;
    X = vaciley;
    X = kolya;
    X = buddy;
    X = guy;
    X = roma.

woman(X) :-
    X = marina;
    X = maria;
    X = varvara;
    X = nadya;
    X = angela;
    X = petra;
    X = chicken;
    X = girl;
    X = lena.

parent(sasha,vova).
parent(sasha,marina).

parent(grisha,vova).
parent(grisha,marina).

parent(petra,vova).
parent(petra,angela).

parent(angela,buddy).
parent(angela,chicken).

parent(marina,vasiliy1).
parent(marina,maria).

parent(vova,vasiliy2).
parent(vova,varvara).

parent(kolya,vasiliy2).
parent(kolya,varvara).

parent(roma,kolya).
parent(roma,nadya).

parent(lena,kolya).
parent(lena,nadya).

parent(nadya,guy).
parent(nadya,girl).

wife(buddy,chicken).
wife(vasiliy1,maria).
wife(vasiliy2,varvara).
wife(guy,girl).
wife(kolya,nadya).
wife(vova,marina).
wife(vova,angela).

father(X,Y) :- parent(X,Y), man(Y).
mother(X,Y) :- parent(X,Y), woman(Y).
child(X,Y) :- parent(Y,X).
son(X,Y) :- child(X,Y), man(Y).
daughter(X,Y) :- child(X,Y), woman(Y).
siblings(X,Y) :- father(X,A), father(Y,A), mother(X,B), mother(Y,B), X\=Y.
brother(X,Y) :- siblings(X,Y), man(Y).
sister(X,Y) :- siblings(X,Y), woman(Y).
grandparent(X,Y) :- parent(X,Z), parent(Z,Y).
grandmother(X,Y) :- grandparent(X,Y), woman(Y).
grandfather(X,Y) :- grandparent(X,Y), man(Y).
uncle(X,Y) :- parent(X,Z), brother(Z,Y).
aunt(X,Y) :- parent(X,Z), sister(Z,Y).
cousin(X,Y) :-
    uncle(X,Z), child(Z,Y)
    ;
    aunt(X,Z), child(Z,Y).
stepcousin(X,Y) :- father(X,A), father(Y,A), mother(X,B), \+ mother(Y,B), X\=Y.
